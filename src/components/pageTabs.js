import React, { useEffect, useState } from 'react';
import {Tabs, Tab, AppBar} from '@material-ui/core';
import Support from './support';
import './pageTabs.scss';

const PageTabs = () => {
    const [tabValue, setTabValue] = useState(0);
    
    const handleTabs = (e, val) => {
        setTabValue(val);
    };

    return(
        
        <div className="page">
            <Tabs className="tabs" onChange={handleTabs} value={tabValue} orientation="vertical" variant="scrollable">
                    <Tab label="Large" />
                    <Tab label="Medium" />
                    <Tab label="Small" />
            </Tabs>

            <TabPanel value={tabValue} index={0}>
                <div className="container-lg">
                    <Support />
                </div>
            </TabPanel>
            <TabPanel value={tabValue} index={1}>
                <div className="container-md">
                    <Support />
                </div>
            </TabPanel>
            <TabPanel value={tabValue} index={2}>
                <div className="container-sm">
                    <Support />
                </div>
            </TabPanel>
        </div>
    )
}

const TabPanel = (props) => {
    const {children, value, index} = props;

    return(
        <div>
            {value===index && (
                <div>
                    {children}
                </div>
            )}
        </div>
    )
};

export default PageTabs;