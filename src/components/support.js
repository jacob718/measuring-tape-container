import React, { useEffect, useState } from 'react';
import MeasureUIMount from '../microuis/measure-mount';
import './support.scss';

const Support = () => {

    return (
        <div>
            <MeasureUIMount />
        </div>

    )
}

export default Support;