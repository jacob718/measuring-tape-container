import { useRef } from 'react';
import { mount } from 'measure/Measure';
import React, { useEffect } from 'react';

// eslint-disable-next-line react/prop-types
const MeasureUIMount = () => {
  const ref = useRef(null);
  useEffect(() => {
    mount(ref.current);
  });
  return <div ref={ref}></div>;
};


export default MeasureUIMount;