import React from 'react';
import PageTabs from './components/pageTabs'

function App() {
  return (
    <div className="App">
      <PageTabs />
    </div>
  );
}

export default App;
