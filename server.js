const express = require('express');
const dotenv = require('dotenv');

// Create server
const app = express();
app.use(express.static('dist'));
const PORT = process.env.PORT || 3000;

const server = app.listen(PORT, console.log(`Server running in ${process.env.NODE_ENV} mode on port ${PORT}`));